#!/bin/bash

echo "deb http://tweedo.com/mirror/mariadb/repo/5.5/debian wheezy main" > /etc/apt/sources.list.d/mariadb.list
echo "deb http://packages.dotdeb.org squeeze all" > /etc/apt/sources.list.d/php53.list
echo "deb http://ftp.de.debian.org/debian/ squeeze main" >> /etc/apt/sources.list.d/php53.list

# testbotmaster.devdrupal.org break php 5.3.27 updating it to 5.4.4
echo "deb http://testbotmaster.devdrupal.org/packages stable  main" >> /etc/apt/sources.list.d/drupaltestbot.list

apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
apt-get -y update
aptitude install libmysqlclient18=5.5.32+maria-1~wheezy mariadb-client-5.5 mariadb-server-5.5
#apt-get -y install libaio1 libdbd-mysql-perl libdbi-perl libhtml-template-perl libmariadbclient18 libmysqlclient18 libnet-daemon-perl  libplrpc-perl mariadb-client-5.5 mariadb-client-core-5.5 mariadb-common mariadb-server mariadb-server-5.5 mariadb-server-core-5.5 mysql-common

apt-get -y remove --purge `dpkg -l | grep php | grep -w 5.4 | awk '{print $2}' | xargs`

#VERSION=`apt-cache showpkg php5|grep dotdeb| grep 5.3 |tail -n 1| awk '{print $1}'`
VERSION="5.3.27-1~dotdeb.0"

apt-get -y --force-yes install php5=$VERSION php5-cli=$VERSION php5-common=$VERSION php5-mysql=$VERSION libapache2-mod-php5=$VERSION
apt-get -y --force-yes install libapache2-mod-php5filter=$VERSION php5-cgi=$VERSION php5-curl=$VERSION php5-dbg=$VERSION php5-dev=$VERSION php5-enchant=$VERSION php5-fpm=$VERSION php5-gd=$VERSION php5-gmp=$VERSION php5-imap=$VERSION php5-interbase=$VERSION php5-ldap=$VERSION php5-mcrypt=$VERSION php5-odbc=$VERSION php5-pgsql=$VERSION php5-pspell=$VERSION php5-recode=$VERSION php5-snmp=$VERSION php5-sqlite=$VERSION php5-sybase=$VERSION php5-tidy=$VERSION php5-xmlrpc=$VERSION php5-xsl=$VERSION php-pear=$VERSION
apt-get -y --force-yes install telnet lynx strace mailutils xtail htop postfix sudo openssh-server git drush 
mkdir -p /var/cache/git/reference
git --git-dir /var/cache/git/reference init --bare

# drupaltestbot break php 5.3.27 updating it to 5.4.4
apt-get -y --force-yes install drupaltestbot
