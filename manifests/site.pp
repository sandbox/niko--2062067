import "base"

# Default is mysql build.
node default {
  include base
  include testing_bot
  include testing_bot::mysql
}

# If the name of the server contains -pgsql, use the PostgreSQL profile.
node /pgsql/ {
  include base
  include testing_bot
  include testing_bot::pgsql
}

# If the name of the server contains -sqlite3, use the SQLite profile.
node /sqlite3/ {
  include base
  include testing_bot
  include testing_bot::sqlite3
}
