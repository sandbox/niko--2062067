#
# Explicit stage for pre-initialization tasks.
#
# Some tasks need to be executed before everything else (especially APT
# repository configuration). We define an explicit stage here to avoid
# dependency issues.
#
stage { "base-prepare": before => Stage[main] }
stage { "base-prealable": before => Stage[base-prepare] }
stage { "base-final": }
Stage['main'] -> Stage['base-final']

# Parent class for all systems.
# Configure APT repositories and ensure freshness of packages.
class base {

  file { "/etc/puppet/puppet.conf":
    source => "puppet:///modules/base/etc/puppet/puppet.conf",
    notify => Service["puppet"],
  }

  file { "/etc/default/puppet":
    source => "puppet:///modules/base/etc/default/puppet",
    owner => 'root',
    group => 'root',
    notify => Service['puppet'],
  }

  service { "puppet":
    enable => true,
    ensure => running,
  }

  class { "base::apt_prepare": stage => "base-prealable" }
  class { "base::apt::standard": stage => "base-prepare" }
  class { "base::final": stage => "base-final" }

  # Firewall configuration.
  include "firewall"

  # Locales configuration.
  file { "/etc/locale.gen":
    source => "puppet:///modules/base/etc/locale.gen",
    require => Package["locales"],
    notify => Exec["locale-gen"],
    owner => 'root',
    group => 'root',
  }
  file { "/etc/default/locale":
    source => "puppet:///modules/base/etc/default/locale",
    require => Package["locales"],
    owner => 'root',
    group => 'root',
  }
  exec { "locale-gen":
    path        => "/usr/bin:/bin:/usr/sbin:/sbin",
    command     => "locale-gen",
    refreshonly => true,
  }
  package { "locales": }

  # Generally needed packages
  package { ["telnet", "lynx", "strace", "mailutils", "xtail", "htop", "postfix", "sudo", "openssh-server", ]: }

  # Login configuration.
  file { "/etc/login.defs":
    owner   => root,
    group   => root,
    mode    => 644,
    source  => "puppet:///modules/base/login.defs",
  }

  file { "/etc/sudoers":
    owner   => root,
    group   => root,
    mode    => 440,
    source  => "puppet:///modules/base/etc/sudoers",
    require => Package["sudo"],
  }

  # SSH configuration.
  service { "ssh":
    pattern => "/usr/sbin/sshd",
    hasrestart => true,
    hasstatus => true,
    require => Package["openssh-server"]
  }
  file { "/etc/ssh/sshd_config":
    source => "puppet:///modules/base/etc/ssh/sshd_config",
    require => Package["openssh-server"],
    notify => Service["ssh"],
    owner => 'root',
    group => 'root',
  }

  file { ["/var/cache/git", "/var/cache/git/reference"]:
    ensure => directory,
  }

  exec { initialize_git_cache:
    command => "/usr/bin/git --git-dir /var/cache/git/reference init --bare",
    creates => "/var/cache/git/reference/config",
    require => File['/var/cache/git/reference'],
    timeout => 0,
  }

  # Standard default database dump
  file { "/tmp/drupaltestbot.sql.gz":
    source => "puppet:///modules/base/drupaltestbot.sql.gz",
  }

  # Load the database from the starter file *if* we don't already have tables
  # populated in it.
  exec { "dbload":
    command => "test $(drush -r /var/lib/drupaltestbot sqlq 'show tables;' | wc -l) -gt 0 || (touch /etc/drupaltestbot/.db_loaded &&  gzip -dc /tmp/drupaltestbot.sql.gz | mysql drupaltestbot)",
    require => [Package['drupaltestbot'], File['/tmp/drupaltestbot.sql.gz'], Pear['drush']],
    creates => "/etc/drupaltestbot/.db_loaded",
    path        => "/usr/bin:/bin:/usr/sbin:/sbin",
  }

  exec { "drush-set-sitename":
    command => "drush -r /var/lib/drupaltestbot vset site_name 'testbot: ${hostname} (${ipaddress})' && touch /etc/drupaltestbot/.site_name_set",
    require => Exec['dbload'],
    creates => "/etc/drupaltestbot/.site_name_set",
    path => "/usr/bin:/bin:/usr/sbin:/sbin",
  }

  exec { "drush-set-https-qa":
    command => "drush -r /var/lib/drupaltestbot vset pifr_client_server https://qa.drupal.org/ && touch /etc/drupaltestbot/.qa_url_set",
    require => Exec['dbload'],
    creates => "/etc/drupaltestbot/.qa_url_set",
    path => "/usr/bin:/bin:/usr/sbin:/sbin",
  }

# Maintainers keys
  file { "/root/.ssh":
    ensure   => directory,
    owner    => root,
    group    => root,
    mode     => 0700,
  }

  # Although authorized_keys2 is deprecated, it still works everywhere I know
  # As a result we can use *it* as the puppet-managed version of authorized_keys
  # and /root/authorized_keys as the box-managed version.
  file { "/root/.ssh/authorized_keys2":
    source => "puppet:///modules/base/root/.ssh/authorized_keys2",
    owner => root,
    group => root,
    mode => 0600,
  }

  # Firewall configuration.
  firewall::rule::allow_servers { "ssh":
    protocol => tcp,
    port => ssh,
    servers => [ "0.0.0.0/0" ],
  }

  file { "/usr/local/bin":
    ensure => present,
    owner => root,
    group => root,
    mode => 0755,
    recurse => true,
    purge => false,
    source => "puppet:///modules/base/usr/local/bin",
  }

  # This is a hopefully temporary hack dependency on the named host being
  # a server for qa. It tries to bypass nginx, which seems to be preventing
  # transactions of some sizes.
  # https://drupal.org/node/2009884
  exec { "hosts_qa_fix":
    command => '/bin/echo "
    # Pass qa directly to www2 to avoid nginx
    140.211.10.17 qa.drupal.org
    " >> /etc/hosts',
    onlyif => '/usr/bin/test 0 -eq `/bin/grep -c qa.drupal.org /etc/hosts`',
  }

}

class base::apt_prepare {
  exec { "apt-update":
    command => "/usr/bin/apt-get update && touch /var/tmp/.apt-update_done",
    timeout => 0,
    creates => "/var/tmp/.apt-update_done",
  }

  package { "debian-archive-keyring":
    ensure => latest,
    require => Exec["apt-update"],
  }

  # Change content of base/files/etc/upgrade_initiator in any way you want to
  # when an upgrade should be performed.
  # From http://www.memonic.com/user/pneff/folder/55756627-f51c-43f0-adfd-777635574108/id/1Z9999x
  file { "/etc/upgrade_initiator":
    source => "puppet:///modules/base/etc/upgrade_initiator",
  }

  exec { "/usr/bin/apt-get -y upgrade":
    refreshonly => true,
    subscribe => File["/etc/upgrade_initiator"],
    environment => "DEBIAN_FRONTEND=noninteractive",
    require => Exec["apt-update"],
    timeout => 0,
  }

  package { 'exim4-base':
    ensure => absent,
  }

}

#
# Standard APT configuration for the test bots.
#
class base::apt::standard {
  include base::apt

  base::apt::repository { "squeeze":
    repository_source => "puppet:///modules/base/squeeze.sources.list",
  }

  base::apt::repository { "squeeze-backports":
   repository_source => "puppet:///modules/base/squeeze-backports.sources.list",
  }

  base::apt::repository { "maria":
    repository_source => "puppet:///modules/base/maria.sources.list",
    key_source => "puppet:///modules/base/maria.public.key",
    key_id => "1BB943DB",
  }

  base::apt::repository { "testbotmaster":
    repository_source => "puppet:///modules/base/testbot.sources.list",
    key_source => "puppet:///modules/base/testbot.public.key",
    key_id => "A19A51A2",
  }
}

class base::final {
  # Any change to testbot_cleanup.sh will result in it being run on the testbots.
  exec { "/usr/local/bin/testbot_cleanup.sh":
    refreshonly => true,
    subscribe => File["/usr/local/bin/testbot_cleanup.sh"],
    environment => "DEBIAN_FRONTEND=noninteractive",
    require => File["/usr/local/bin/testbot_cleanup.sh"],
  }
  file { "/usr/local/bin/testbot_cleanup.sh":
    source => "puppet:///modules/base/usr/local/bin/testbot_cleanup.sh",
    mode => 0777,
    require => File['/usr/local/bin'],
  }
}
