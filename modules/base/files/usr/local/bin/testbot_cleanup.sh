#!/bin/bash -x

# You can do anything you want as a cleanup or fixup here
# Any time this file changes it will be rerun on all testbots.

cd /var/lib/drupaltestbot/sites/all/modules/project_issue_file_review && git pull

drush -r /var/lib/drupaltestbot vset pifr_client_server https://qa.drupal.org/

touch /etc/drupaltestbot/.db_loaded

date >>/tmp/testbot_cleanup_ran.txt
